package ru.sberbank.jd.dto;

import lombok.*;


@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReferenceDataType {


    private Character characterValue;
    private Boolean booleanValue;
    private Byte byteValue;
    private Short shortValue;
    private Integer integerValue;
    private Long longValue;
    private String stringValue;
    private Float floatValue;
    private Double doubleValue;



}
