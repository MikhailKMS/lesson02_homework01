package ru.sberbank.jd;

import ru.sberbank.jd.dto.ReferenceDataType;

public class Main {

    public static void main(String[] args) {

        ReferenceDataType referenceDataType = new ReferenceDataType();

        System.out.println(referenceDataType);


        referenceDataType.setCharacterValue('H');
        referenceDataType.setBooleanValue(false);
        referenceDataType.setByteValue(new Byte("127"));
        referenceDataType.setByteValue((byte)0);
        referenceDataType.setShortValue(new Short("32767"));
        referenceDataType.setShortValue((short)32767);
        referenceDataType.setIntegerValue(2147483647);
        referenceDataType.setLongValue(9223372036854775807L);
        referenceDataType.setStringValue("Hello");
        referenceDataType.setFloatValue(1.5F);
        referenceDataType.setDoubleValue(1.5D);

        System.out.println(referenceDataType);

        ReferenceDataType referenceDataType1 = new ReferenceDataType('A', true, (byte)1, (short)2, 10, 6L, "Bye", 1F, 1D);
        System.out.println(referenceDataType1);

    }


}
